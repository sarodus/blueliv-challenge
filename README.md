Blueliv Threat api/scraper
==========================

# Installation prerequisites

- docker
- docker-compose

# Development set up

**IMPORTANT**

Copy .env.example file to .env

In the .env.example there are some example variables, these should only be used as an example.

Now we can create the database with

`make migrate`

Then we can run it!

`make up`

# Usage

`make up`

This will launch both api service and spider, then go to 

http://localhost:8000/
or
http://localhost:8000/threats/


# TIPS

To make things easy, there's a Makefile with lot of commands like entering to docker with bash, start, stop, etc... see Makefile for more details

|        Command        |                        Description                          |
|-----------------------|-------------------------------------------------------------|
| make bash             | Enter to API container with bash                            |
| make stop             | Stop all containers                                         |
| make up               | Start all containers                                        |
| make restart          | Restart all containers                                      |
| make ps               | Show the status of all containers                           |
| make kill             | Force quit all containers                                   |
| make logs             | Tail logs of all containers                                 |
| make migrate          | Run Django migrations                                       |
| make test             | Run tests of API                                            |
| make run_spider       | Launch the scraper spider                                   |
| make psql             | Enter to PostgreSQL (you're ask for the db password)        |
| make truncate_threats | Truncate the threats table (you're ask for the db password) |

# Requirements answers

- The software must be able scale to millions of users requesting the list of
occurrences, but scraping will only be done once a day.
    - As we we can place as many as instances of the API, it scales horizontally, but if the database the bottleneck we could do some layer of cache with Redis or Memcached

- API must be secure
    - Our API is secured by the gateway, is only accessible on our private network, and can use the JWT to know the user information

- Explain how you would provide visibility in production for Errors, Metrics and Alerts.
    - We'd have to make an integration of the API/Scraper with some systems like Kibana and Sentry, there are libraries to integrate easily

- Show a diagram with named components of the production infrastructure to be able to scale

![Alt text](./diagram.svg)


# Technical explanation of the scraper

Since https://community.blueliv.com/#!/discover is an app done with AngularJS, and the navigation in on the frontend, we could scrap this in multiple ways explained below.

I'd discuss which version to implement in the real world, but I've implemented the harder one just for the example

Also, the login is not required since we already can access the threats, but I've implemented it anyway for the example


## Simulating user navigates with clicks (harder)
Program selenium to do all the clicks like the user would do

PROS:

- Harder to detect
- If the web's API changes, but the page doesn't, we're ok

CONS:

- Longer code
- If the page changes, we'd have to fix it
- The field created_at is formatted with momentjs, so it's hard to parse (I've done an implementation of reading the AngularJS state to get it)

## Simulating the requests the navigator would do
Here we'd hard-code the call to login then to the API which the AngularJS is accessing directly

https://community.blueliv.com/privateapi/v1/sparks/discover?limit=10&offset=0


PROS:

- Super simple code

- If the web changes, but the API still exists, we're ok

CONS:

- If the API changes, we'd have to fix it

- Easier to get caught
    - Maybe we could put similar headers as the real call would do (XHR, etc...)
