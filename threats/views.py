from django.shortcuts import render
from rest_framework import viewsets
from .models import Threat
from .serializers import ThreatSerializer


class ThreatViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ThreatSerializer
    queryset = Threat.objects.all()
