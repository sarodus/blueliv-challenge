from rest_framework import serializers
from .models import Threat


class ThreatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Threat
        fields = [
            "title",
            "body",
            "sources",
            "created_at",
            "username",
        ]
