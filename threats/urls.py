from rest_framework.routers import DefaultRouter
from .views import ThreatViewSet


router = DefaultRouter(trailing_slash=False)
router.register(r"threats/?", ThreatViewSet)
urlpatterns = router.urls
