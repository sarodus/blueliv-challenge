from django.contrib.postgres.fields import ArrayField
from django.db import models


class Threat(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    sources = ArrayField(
        models.CharField(max_length=255, blank=True),
        blank=True,
        null=True,
    )
    username = models.CharField(max_length=255)
