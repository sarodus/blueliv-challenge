# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import exists
from .models import db_connect, create_table, Threat


class ScraperPipeline:
    def __init__(self):
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()
        title = item["title"]

        if not session.query(exists().where(Threat.title == title)).scalar():
            threat = Threat(**item)
            session.add(threat)
            session.commit()

        return item
