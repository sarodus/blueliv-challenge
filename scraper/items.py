# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ThreatItem(scrapy.Item):
    title = scrapy.Field()
    body = scrapy.Field()
    sources = scrapy.Field()
    username = scrapy.Field()
    created_at = scrapy.Field()

    def is_threat(self):
        search_keyword = "threat"

        if (
            search_keyword in self["title"].lower()
            or search_keyword in self["body"].lower()
            or any(
                search_keyword in source.lower() for source in self["sources"]
            )
        ):
            return True
        return False
