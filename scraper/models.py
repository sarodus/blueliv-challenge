from sqlalchemy import create_engine, Column, Table
from sqlalchemy import Integer, String, Date, DateTime, Text
from sqlalchemy.types import ARRAY
from sqlalchemy.ext.declarative import declarative_base
from scrapy.utils.project import get_project_settings

Base = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(get_project_settings().get("CONNECTION_STRING"))


def create_table(engine):
    Base.metadata.create_all(engine)


class Threat(Base):
    __tablename__ = "threats_threat"

    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    body = Column(Text())
    created_at = Column(DateTime)
    sources = Column(ARRAY(String))
    username = Column(String(255))
