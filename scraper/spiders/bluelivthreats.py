import scrapy
from time import sleep

from scrapy_selenium import SeleniumRequest
from scrapy.utils.project import get_project_settings
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from ..items import ThreatItem


class BluelivthreatsSpider(scrapy.Spider):
    name = "bluelivthreats"
    allowed_domains = ["community.blueliv.com"]
    start_urls = ["https://community.blueliv.com/"]

    cookies_popup_btn_css = ".cookies-popup button"
    login_button_css = '.sidebar-profile a[alt="Log in"] span'
    submit_login_btn_css = 'button[alt="Log in"]'
    email_input_css = 'input[type="email"]'
    pwd_input_css = 'input[type="password"]'
    articles_xpath = "//article"
    discovery_a_css = 'a[href="#!/discover"]'

    def start_requests(self):
        yield SeleniumRequest(
            url=self.start_urls[0],
            callback=self.main,
            wait_time=10,
            wait_until=EC.element_to_be_clickable(
                (By.CSS_SELECTOR, self.login_button_css)
            ),
        )

    def main(self, response):
        driver = response.request.meta["driver"]

        self._remove_cookies_popup(driver)

        self._do_login(driver)  # we could skip this since this is public

        self._go_to_discovery(driver)

        yield from self._extract_articles(driver)

    def _extract_articles(self, driver):
        self._wait_until_articles_visible(driver)
        titles_to_dates_map = self._extract_article_dates_from_js(driver)
        articles = driver.find_elements_by_xpath(self.articles_xpath)

        for i, article in enumerate(articles):
            if i >= 5:
                # only inspect first 5 items
                break

            title = article.find_element_by_css_selector("h2").text
            body = article.find_element_by_css_selector("p").text
            sources = [
                a.get_attribute("href")
                for a in article.find_elements_by_css_selector(".box-sources a")
            ]
            username = article.find_element_by_css_selector(
                "user_avatar ~ span"
            ).text
            created_at = titles_to_dates_map.get(title)
            yield ThreatItem(
                title=title,
                body=body,
                sources=sources,
                username=username,
                created_at=created_at,
            )

    def _remove_cookies_popup(self, driver):
        driver.find_element_by_css_selector(self.cookies_popup_btn_css).click()

    def _do_login(self, driver):
        driver.find_element_by_css_selector(self.login_button_css).click()
        driver.find_element_by_css_selector(self.email_input_css).send_keys(
            get_project_settings().get("COMMUNITY_BLUELIV_USERNAME")
        )
        driver.find_element_by_css_selector(self.pwd_input_css).send_keys(
            get_project_settings().get("COMMUNITY_BLUELIV_PASSWORD")
        )
        # the email input have a debounce of 1000ms, we have to wait it
        sleep(1)

        driver.find_element_by_css_selector(self.submit_login_btn_css).click()

    def _go_to_discovery(self, driver):
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self.discovery_a_css))
        )
        driver.find_element_by_css_selector(self.discovery_a_css).click()

    def _wait_until_articles_visible(self, driver):
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, self.articles_xpath))
        )

    def _extract_article_dates_from_js(self, driver):
        """
        created_at is formatted with momentjs like "an hour ago"
        so we can try to inspect the state of tha angularjs page to extract
        this information with date format
        """
        try:
            script = (
                "return angular.element('section').scope().vm.sds.getItems()"
            )
            output = driver.execute_script(script)
            titles_to_dates_map = {
                row["title"]: row["created_at"] for row in output
            }
            return titles_to_dates_map
        except Exception:
            print("Could not get created_at data")
        return {}
