bash:
	docker-compose run --rm api bash

stop:
	docker-compose stop

up:
	docker-compose up

restart:
	docker-compose restart

ps:
	docker-compose ps

kill:
	docker-compose kill

logs:
	docker-compose logs -f --tail=100 

migrate:
	docker-compose run --entrypoint python --rm api manage.py migrate

test:
	docker-compose run --entrypoint python --rm api manage.py test

run_spider:
	docker-compose run --entrypoint scrapy --rm scraper crawl bluelivthreats

psql:
	docker-compose run --entrypoint psql --rm db -h db -p 5432 -U postgres -d postgres

truncate_threats:
	docker-compose run --entrypoint psql --rm db -h db -p 5432 -U postgres -d postgres -c 'TRUNCATE TABLE threats_threat;'
