from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from threats.models import Threat


class ThreatListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Threat.objects.create(
            title="Example title",
            body="Example body",
            created_at=datetime.now(tz=timezone.utc),
            username="@example_username",
        )

    def test_view_url_exists_at_desired_location(self):
        resp = self.client.get("/threats")
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse("threat-list"))
        self.assertEqual(resp.status_code, 200)

    def test_lists_all_threats(self):
        resp = self.client.get(reverse("threat-list"))
        self.assertEqual(resp.status_code, 200)
        data = resp.json()
        self.assertEqual(len(data), 1)
